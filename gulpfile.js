var gulp = require('gulp');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var cleanCSS = require('gulp-clean-css');

var dsource = "./src/";
var ddest = "./www/";

var srcfiles = [dsource+'sass/styles.scss'];
var srcfiles_watch = [dsource+'sass/**/*.scss'];

w = process.cwd();
styles = gulp.task(
  'styles',
  function() {
    return gulp.src(srcfiles)
    .pipe(sass().on('error', sass.logError))
    .pipe(rename("styles.css"))
    .pipe(gulp.dest(ddest+'css/'))
  });

var bootstrapjs = {
  src: [
    'javascript/transition.js',
    'javascript/alert.js',
    'javascript/button.js',
    'javascript/carousel.js',
    'javascript/collapse.js',
    'javascript/dropdown.js',
    'javascript/modal.js',
    'javascript/tooltip.js',
    'javascript/popover.js',
    'javascript/scrollspy.js',
    'javascript/tab.js',
    'javascript/affix.js'
  ],
  dir: './node_modules/bootstrap-sass/assets/'
};
var jqueryjs = {
  dir: './node_modules/jquery/'
};

gulp.task(
  'build-bootstrap',
  function() {
    gulp.src([bootstrapjs['dir']+'js/*.js'])
      .pipe(concat('bootstrap.js'))
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./www/js/'))
  });
gulp.task(
  'build-jquery',
  function() {
    gulp.src([jqueryjs['dir']+'dist/jquery.min.js'])
      .pipe(gulp.dest('./www/js/'))
  });

gulp.task('default', ['styles']);
gulp.task('watch', function() {
  gulp.watch(srcfiles_watch, ['styles']);
});
